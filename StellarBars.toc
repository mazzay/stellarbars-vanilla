## Interface: 11200
## Title: StellarBars
## Notes: Adds a new look and some options to the default action bars.
## Author: TotalPackage
## Version: 3.3.002

## SavedVariables: StellarBarsDB
## SavedVariablesPerCharacter: StellarBarsCharDB

## OptionalDeps: Ace2, LibSharedMedia-3.0, ButtonFacade, LibButtonFacade
## X-Embeds: Ace2, LibSharedMedia-3.0
## X-Category: Action Bars

## ACE
libs\AceLibrary\AceLibrary.lua
libs\AceOO-2.0\AceOO-2.0.lua
libs\AceHook-2.1\AceHook-2.1.lua
libs\AceEvent-2.0\AceEvent-2.0.lua
libs\AceAddon-2.0\AceAddon-2.0.lua

## Backported code
Compat.lua

## SM
libs\LibSharedMedia-3.0\LibStub\LibStub.lua
libs\LibSharedMedia-3.0\CallbackHandler-1.0\CallbackHandler-1.0.lua
libs\LibSharedMedia-3.0\LibSharedMedia-3.0\LibSharedMedia-3.0.lua

StellarBars.lua
