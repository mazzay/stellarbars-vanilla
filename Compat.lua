_G = getfenv(0)

function InCombatLockdown() 
  return UnitAffectingCombat("player")
end

function PetActionBarFrame_IsAboveShapeshift(val) 
  return true
end

function print(msg)
  if (DEFAULT_CHAT_FRAME) then
    DEFAULT_CHAT_FRAME:AddMessage (msg);
  end
end

function tablesize(T)
  local count = table.getn(T);
  if (count == nil) then count=0;
  end
  return count;
end;

function strmatch(str, template)
  local fIndex, lIndex = string.find(str, "(%d+)")
	local substr = string.sub(str, fIndex, lIndex)
  return substr;
end;

function select (index, ...)
  local tbl = arg
  if type(arg[1]) == "table" and arg[2] == nil then
			tbl = arg[1]
  end
  if index == "#" then
		return tbl and table.getn(tbl) or 0
  else
		return tbl and tbl[index]
  end
end